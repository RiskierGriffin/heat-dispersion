def evolve_heat_slow(u):
    array = []
    array = u.copy()
    for row in range(0, len(array) - 1):
        for column in range(0, len(array[row]) - 1):
            if row != 0 and row != (len(array) - 1):
                if column != 0 and column != (len(array[row]) - 1):
                    s1 = u[row - 1][column]
                    s2 = u[row][column + 1]
                    s3 = u[row + 1][column]
                    s4 = u[row][column -1]
                    num = (s1 + s2 + s3 + s4)
                    change = num/4
                    array[row][column] = change
    return array

def evolve_heat_fast(u):
    array = []
    array = u.copy()
    rows = len(u) #number of rows
    col = len(u[0]) #number of columns
    diff_vertical = (rows - 2)
    if col == 2:
        return array
    if rows == 2:
        return array
    array[1:-1,1:-1] += u[1:-1,0:-2]
    array[1:-1,1:-1] += u[1:-1,2:col]
    array[1:-1,1:-1] += u[0:-2,1:-1]
    array[1:-1,1:-1] += u[(rows - diff_vertical):rows,1:-1]
    array[1:-1,1:-1] = (array[1:-1,1:-1] - u[1:-1,1:-1])
    array[1:-1,1:-1] = ((array[1:-1,1:-1])/4)
    return array
