Given a 2D array of heat-values (at fixed boundary), produces
the new heat distribution after one iteration of the finite
difference method.

Parameters
----------
u : numpy.ndarray shape=(M, N)
An MxN array of heat values at time-step t-1.
(M and N are both at least 2)

Returns
-------
numpy.ndarray, shape=(M, N)
An MxN array of heat values at time-step t.
